import './App.css';
import React from 'react'
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom"
import NotFound from './pages/NotFound'
import LoginPage from './pages/LoginPage'
import AddPost from './pages/AddPost'
import Posts from './pages/Posts'
import Post from './pages/Post'
import SignUp from './pages/SignUp'

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/login" exact component={LoginPage}/>
          <Route path="/signup" exact component={SignUp}/>
          {localStorage.getItem("user") ? (
            <React.Fragment>
              <Route path="/" exact component={Posts}/>
              <Route path="/posts/:id" exact component={Post}/>
              <Route path="/add-post" exact component={AddPost}/>
            </React.Fragment>
          ) : (
            <Redirect to="/login" />
          )}
          <Route path="/404" component={NotFound} />
          <Redirect to="/404" />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
