export function checkInput(input, length) {
  if(input.length > length) {
    alert("The input field must be less than " + length + " characters!!!" )
    return false
  }
  return true
}

export function checkUserName(username) {
  if(username.length < 6) {
    alert("Username must be at least 6 characters")
    return false
  } else {
    alert("Sign up successfully!!!")
    return true
  }
} 