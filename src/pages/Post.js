import React, {useState, useEffect} from 'react'
import Button from '../components/Button'
import { useParams } from "react-router-dom"
import { BASE_URL } from '../constance/url'
import Comment from '../components/Comment'
import { checkInput } from '../utils/checkInput'
import { headers } from '../constance/axios'

function Post(props) {
  const [comment, setComment] = useState([])
  const [commentText, setCommentText] = useState([])
  const [name, setName] = useState("")
  const [post, setPost] = useState({})
  const user = JSON.parse(localStorage.getItem("user"))
  let { id } = useParams();

  useEffect(() => {
    fetch(BASE_URL + "post/" + id,
      {
        headers
      }
    ).then((res) => {
      return res.json()
    }).then((data) => {
      setPost(data)

      fetch(BASE_URL + "user/" + data.userId, {
        headers
      }).then((res) => {
        return res.json()
      }).then((data) => {
        if(!data.name || data.name.length === 0) {
          setName("Anonymous")
        }
        else {setName(data.name)}
      })
    }).catch(() => {
      alert("Failed to get post with id " + id)
    })

    fetch(BASE_URL + "comments",
      {
        headers
      }
    ).then((res) => {
      return res.json()
    }).then((data) => {
      const commentArray = data.filter(c => c.postId == id)
      setComment(commentArray)
    }).catch(() => {
      alert("Failed to get all comments of the post with id " + window.match.params.id)
    })

    
  }, [id])

  const handleChange = (event) => {
    setCommentText(event.target.value);
  }
  const backToMainPage = () => {
    props.history.push('/')
  }
  const backToPostPost = () => {
    props.history.push('/add-post')
  }
  const deletePost = () => {
    fetch(BASE_URL + "post",
      {
        method: 'DELETE',
        body: JSON.stringify({
          id,
        }),
        headers
      }
    ).then((res) => {
      if(res.status === 200) {
        alert("Delete post successfully")
        props.history.push('/')
      } else {
        alert("Failed to delete the post")
      }
    }).catch(() => {
      alert("Failed to delete the post")
    })
  }
  const postComment = (event) => {
    event.preventDefault()
    if(checkInput(commentText, 255)) {
      fetch(BASE_URL + "comment",
        {
          method: 'POST',
          body: JSON.stringify({
            commentContent: commentText,
            userId: JSON.parse(localStorage.getItem("user")).id,
            postId: id,
            createTime: new Date()
          }),
          headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'Authorization': 'Basic ' + btoa('user:password123')
          }
        }
      ).then((res) => {
        if(res.status === 200){
          window.location.reload()
        } else {
          alert("Post comment failed, try again!")
        }
        
      }).catch(() => {
        alert("Post comment failed, try again!")
      })
    }
  }

  return (
    <div className="post-user-container">
      <Button title="Main page" onClick={backToMainPage} />
      <Button title="New post" onClick={backToPostPost} />
      <h3>{name}</h3>
      <p>
        {post.postContent}
      </p>
        {user.id === post.userId && (
          <Button title="Delete post" onClick={deletePost} />         
        )}
       
      <div className="comments-container">
        {comment.map((c) => (
          <Comment key={c.id} id={c.id} userId={c.userId} comment={c.commentContent} />
        ))}
      </div>
      <h4>Your comment</h4>
      <textarea rows="4" cols="60" type="text" name="post" value={commentText} onChange={handleChange}/>
      <br />
      <Button title="Comment" onClick={postComment} />
    </div>
  )
}

export default Post
