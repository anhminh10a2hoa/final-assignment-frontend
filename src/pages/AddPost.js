import React, {useState} from 'react'
import Button from '../components/Button'
import { BASE_URL } from '../constance/url'
import { checkInput } from '../utils/checkInput'
import { headers } from '../constance/axios'

function AddPost(props) {
  const [post, setPost] = useState("");

  const handleChange = (event) => {
    setPost(event.target.value);
  }

  const backToMainPage = () => {
    props.history.push('/')
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    if(checkInput(post, 255)) {
      fetch(BASE_URL + "post",
        {
          method: 'POST',
          body: JSON.stringify({
            postContent: post,
            userId: JSON.parse(localStorage.getItem("user")).id,
            createTime: new Date()
          }),
          headers
        }
      ).then((res) => {
        if(res.status === 200){
          props.history.push('/')
        } else {
          alert("Add post failed, try again!")
        }
        
      }).catch((err) => {
        alert("Add post failed, try again!")
      })
    }
  }
  return (
    <div>
      <h1>Add post</h1>
      <form onSubmit={handleSubmit}>
        <textarea rows="10" cols="80" type="text" name="post" value={post} onChange={handleChange}/>
        <br /><br />
        <Button title="Back" onClick={backToMainPage} />
        <Button type="submit" title="Add"/>
      </form>
    </div>
  )
}

export default AddPost
