import React, {useState, useEffect} from 'react'
import Button from '../components/Button'
import { BASE_URL } from '../constance/url'
import { headers } from '../constance/axios'

function LoginPage(props) {
  const [id, setId] = useState("");

  const handleChange = (event) => {
    setId(event.target.value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    fetch(BASE_URL + "user/" + id, {headers}).then((res) => {
      return res.json()
    }).then((data) => {
      if(data) {
        localStorage.setItem('user', JSON.stringify(data));
        new Promise(() =>
          setTimeout(alert('Login Successfully!'))
        )
        window.location.href = '/'
      } else {
        alert("Sign in failed, try again!")
      }
    }).catch(() => {
      alert("Sign in failed, try again!")
    })
  }

  const handleSignUp = () => {
    props.history.push('/signup')
  }

  return (
    <div className="login-container">
      <h1>Login</h1>
      <h3>Your need an id to join</h3>
      <form onSubmit={handleSubmit}>
        <label htmlFor="id">Id: </label>
        <input type="number" name="id" value={id} onChange={handleChange}/>
        <br />
        <Button type="submit" title="Join"/>
        <Button title="SignUp" onClick={handleSignUp} />
      </form>
    </div>
  )
}

export default LoginPage
