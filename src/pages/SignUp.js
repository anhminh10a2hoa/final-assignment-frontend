import React, {useState} from 'react'
import Button from '../components/Button'
import { BASE_URL } from '../constance/url'
import { headers } from '../constance/axios'
import { checkUserName } from '../utils/checkInput' 

function SignUp(props) {
  const [username, setUsername] = useState("");
  const handleChange = (event) => {
    setUsername(event.target.value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    if(checkUserName(username)) {
      fetch(BASE_URL + "user",
        {
          method: 'POST',
          body: JSON.stringify({
            name: username
          }),
          headers
        }
      ).then((res) => {
        return res.json()
      }).then((data) => {
        new Promise(() =>
          setTimeout(alert("Your username is: " + data.name + " and your id is " + data.id + ". Now your your id to login!!!"))
        )
        props.history.push('/login')
      }).catch((err) => {
        alert("Sign in failed, try again!")
      })
    }
  }

  const handleLogin = () => {
    props.history.push('/login')
  }

  return (
    <div className="login-container">
      <h1>Sign up</h1>
      <h3>Sign up with your username</h3>
      <form onSubmit={handleSubmit}>
        <label htmlFor="username">Username: </label>
        <input type="text" name="username" value={username} onChange={handleChange}/>
        <br />
        <Button type="submit" title="Sign up"/>
        <Button title="Log in" onClick={handleLogin} />
      </form>
    </div>
  )
}

export default SignUp
