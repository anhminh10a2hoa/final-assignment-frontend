import React, {useState,useEffect} from 'react'
import Post from '../components/Post'
import Button from '../components/Button'
import { BASE_URL } from "../constance/url"
import { headers } from '../constance/axios'

function Posts(props) {
  const [data, setData] = useState([])

  const backToMainPage = () => {
    props.history.push('/')
  }

  const backToPostPost = () => {
    props.history.push('/add-post')
  }

  const logout = () => {
    localStorage.removeItem('user')
    props.history.push('/login')
  }
   
  useEffect(() => {
    fetch(BASE_URL + "posts",
    {
      headers
    }
    ).then((res) => {
      return res.json()
    }).then((data) => {
      setData(data)
    })
    .catch(() => {
      alert("Failed to get all the posts")
    })
  }, [])

  return (
    data && (
      <div>
        <Button title="Main page" onClick={backToMainPage} />
        <Button title="Add post" onClick={backToPostPost} />
        <Button title="Log out" onClick={logout} />
        <h1>Posts</h1>
        {data.map((d) => (
          <Post key={d.id} id={d.id} userId={d.userId} content={d.postContent}/>
        ))}  
      </div>
    ) 
  )
}

export default Posts
