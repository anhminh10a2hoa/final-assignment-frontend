import React, {useEffect, useState} from 'react'
import Button from '../components/Button'
import { BASE_URL } from '../constance/url'
import { headers } from '../constance/axios'

function Comment(props) {
  const [commentOwner, setCommentOwner] = useState("");
  const user = JSON.parse(localStorage.getItem("user"))

  useEffect(() => {
    fetch(BASE_URL + "user/" + props.userId, {
      headers
    }).then((res) => {
      return res.json()
    }).then((data) => {
      if(!data.name || data.name.length === 0) {
        setCommentOwner("Anonymous")
      }
      else {setCommentOwner(data.name)}
    })
  }, [props.userId])

  const deleteComment = () => {
    fetch(BASE_URL + "comment",
      {
        method: 'DELETE',
        body: JSON.stringify({
          id: props.id,
        }),
        headers
      }
    ).then((res) => {
      if(res.status === 200) {
        alert("Delete comment successfully")
        window.location.reload();
      } else {
        alert("Failed to delete the comment")
      }
    }).catch(() => {
      alert("Failed to delete the comment")
    })
  }
  
  return commentOwner ? (
    <div className="comment-container">
      <h5>{commentOwner}</h5>
      <p>{props.comment}
      </p>
      {props.userId === user.id && (
        <Button title="Delele comment" onClick={deleteComment} />
      )}
    </div>
  ) : (
    <p>Loading...</p>
  )
}

export default Comment
