import React, {useEffect, useState} from 'react'
import Button from './Button'
import { useHistory } from "react-router-dom"
import { BASE_URL } from "../constance/url"
import { headers } from '../constance/axios'

function Post(props) {
  const history = useHistory()
  const [name, setName] = useState("")
  const handleOnclick = () => {
    history.push('/posts/' + props.id)
  }

  useEffect(() => {
    fetch(BASE_URL + "user/" + props.userId, {
      headers
    }).then((res) => {
      return res.json()
    }).then((data) => {
      if(!data.name || data.name.length === 0) {
        setName("Anonymous")
      }
      else {setName(data.name)}
    })
  }, [props.userId])

  return name ? (
    <div className="post-container">
      <h4>{name}</h4>
      <p>{props.content}</p>
      <Button title="See post" onClick={handleOnclick} />
    </div>
  ) : (
    <div>Loading...</div>
  )
}

export default Post
