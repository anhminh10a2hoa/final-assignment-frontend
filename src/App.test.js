import { render, screen, act } from '@testing-library/react';
import App from './App';
import Posts from './pages/Posts'
import Post from './pages/Post'

test('renders learn react link', () => {

});

describe('Posts', () => {
  it("test buttons", async () => {
    await act(async () => render(<Posts />));
    expect(screen.getByText("Main page")).toBeInTheDocument();
    expect(screen.getByText("Add post")).toBeInTheDocument();
    expect(screen.getByText("Log out")).toBeInTheDocument();
  })
})