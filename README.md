# Final Project Front-end

## How to run the application?

+ Make sure you have already installed Node (install it [here](https://nodejs.org/en/))

+ Clone our repo or download source code 

```batch
git clone https://gitlab.com/anhminh10a2hoa/final-assignment-frontend
```

+ Install packages and its dependencies:

```
npm install
```

+ Run the assignment, it will be run on port 3000:

```
npm start
```

## Main Feature

+ GET/POST/DELETE your own posts
+ GET/POST/DELETE your own comments
+ Login/Sign up your account
+ Using localstorage to store the user information

## Test

+ Test the application:

```
npm test
```

## Deploy

+ I deployed to Heroku and you can access with the Link below:

  [https://final-assignment-frontend.herokuapp.com](https://final-assignment-frontend.herokuapp.com)

## How to use the application

+ Step 1: Login to the application using your <b>id</b> (if you don't have account before sign up with your username then the application will inform you your id)

![alt text](https://i.imgur.com/GMhP2BT.png)
![alt text](https://i.imgur.com/ap2LwNu.png)

+ Step 2: Add your own posts by clicking to the "Add post" button on the top of the screen.

+ Step 3: Click to your post and you can delete your own one

+ Step 4: You also can comment to any posts and delete your own comments

### Author

+ Minh Hoang (e1800956) - VAMK
+ Minh Hoang (e1800950) - VAMK